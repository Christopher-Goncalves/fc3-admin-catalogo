package com.fulcycle.catalogo.domain.exeptions;

public class NoStrackTraceExeption extends RuntimeException {

    public NoStrackTraceExeption(final String message) {
        this(message, null);
    }

    public NoStrackTraceExeption(final String message, final Throwable cause) {
        super(message, cause, true, false);
    }
}

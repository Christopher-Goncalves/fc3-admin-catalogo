package com.fulcycle.catalogo.domain.validation.Handler;

import com.fulcycle.catalogo.domain.exeptions.DomainExeption;
import com.fulcycle.catalogo.domain.validation.Error;
import com.fulcycle.catalogo.domain.validation.ValidationHandler;

import java.util.List;

public class ThrowsValidationHandler implements ValidationHandler {
    @Override
    public ValidationHandler append(final Error anError) {
        throw DomainExeption.with(anError);
    }

    @Override
    public ValidationHandler append(ValidationHandler anHandle) {
        throw DomainExeption.with(anHandle.getErrors());
    }

    @Override
    public ValidationHandler validate(Validation anValidation) {
        try {
            anValidation.validate();
        } catch (final Exception ex) {
            throw DomainExeption.with(new Error(ex.getMessage()));
        }
        return this;
    }

    @Override
    public List<Error> getErrors() {
        return null;
    }

    @Override
    public boolean hasErrors() {
        return ValidationHandler.super.hasErrors();
    }
}

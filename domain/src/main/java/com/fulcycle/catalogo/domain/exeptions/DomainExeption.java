package com.fulcycle.catalogo.domain.exeptions;

import com.fulcycle.catalogo.domain.validation.Error;

import java.util.List;


public class DomainExeption extends RuntimeException {
    private final List<Error> errors;

    private DomainExeption(final String aMessage, final List<Error> aErrors) {
        super(aMessage);
        this.errors = aErrors;
    }

    public static DomainExeption with(Error anErrors) {
        return new DomainExeption(anErrors.message(), List.of(anErrors));
    }

    public static DomainExeption with(final List<Error> anErrors) {
        return new DomainExeption("", anErrors);
    }

    public List<Error> getErros() {
        return this.errors;
    }
}

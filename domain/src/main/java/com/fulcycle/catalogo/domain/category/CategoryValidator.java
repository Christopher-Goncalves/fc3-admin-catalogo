package com.fulcycle.catalogo.domain.category;

import com.fulcycle.catalogo.domain.validation.Error;
import com.fulcycle.catalogo.domain.validation.ValidationHandler;
import com.fulcycle.catalogo.domain.validation.Validator;

import java.util.Objects;

public class CategoryValidator extends Validator {
    private final Category category;
    private static final int NAMEM_MAX_LENGTH = 255;
    private static final int NAME_MIN_LRNGTH = 3;
    public CategoryValidator(final Category aCategory, final ValidationHandler aHandler) {
        super(aHandler);
        this.category = aCategory;
    }

    @Override
    public void validate() {
        chackNameConstraints();
    }

    private void chackNameConstraints() {
        final var name = this.category.getName();
        if (Objects.isNull(name)) {
            this.validationHandler().append(new Error("'name' should not be null"));
            return;
        }
        if (name.isBlank()) {
            this.validationHandler().append(new Error("'name' should not be empty"));
            return;
        }
        int length = name.trim().length();
        if (name.length() > NAMEM_MAX_LENGTH || length < NAME_MIN_LRNGTH) {
            this.validationHandler().append(new Error("'name' must be between 3 and 255 characters"));
            return;
        }
    }
}

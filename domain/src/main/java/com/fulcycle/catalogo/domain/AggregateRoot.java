package com.fulcycle.catalogo.domain;

public abstract class AggregateRoot<ID extends Indentifier> extends Entity<ID> {
    protected AggregateRoot(ID id) {
        super(id);
    }
}

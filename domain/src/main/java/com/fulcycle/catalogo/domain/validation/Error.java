package com.fulcycle.catalogo.domain.validation;

public record Error(String message) {
}

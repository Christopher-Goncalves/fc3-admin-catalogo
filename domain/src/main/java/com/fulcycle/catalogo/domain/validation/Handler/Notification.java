package com.fulcycle.catalogo.domain.validation.Handler;

import com.fulcycle.catalogo.domain.exeptions.DomainExeption;
import com.fulcycle.catalogo.domain.validation.Error;
import com.fulcycle.catalogo.domain.validation.ValidationHandler;

import java.util.ArrayList;
import java.util.List;

public class Notification implements ValidationHandler {

    private final List<Error> errors;

    public Notification(List<Error> errors) {
        this.errors = errors;
    }

    public static Notification create(final Error anError) {
        return new Notification(new ArrayList<>()).append(anError);
    }

    public static Notification create(final Throwable throwable) {
        return create(new Error(throwable.getMessage()));
    }

    public static Notification create() {
        return new Notification(new ArrayList<>());
    }


    @Override
    public Notification append(final Error anError) {
        this.errors.add(anError);
        return this;
    }

    @Override
    public Notification append(ValidationHandler anHandle) {
        this.errors.addAll(anHandle.getErrors());
        return this;
    }

    @Override
    public Notification validate(Validation anValidation) {
        try {
            anValidation.validate();
        } catch (final DomainExeption ex) {
            this.errors.addAll(ex.getErros());
        } catch (final Throwable t) {
            this.errors.add(new Error(t.getMessage()));
        }
        return this;
    }

    @Override
    public List<Error> getErrors() {
        return this.errors;
    }
}

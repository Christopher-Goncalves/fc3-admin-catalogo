package com.fulcycle.catalogo.domain.category;

import com.fulcycle.catalogo.domain.pagination.Pagination;

import java.util.Optional;

public interface CategoryGatway {
    Category create(Category aCategory);

    void deleteById(CategoryID anId);

    Optional<Category> findById(CategoryID anId);

    Category update(Category aCategory);

    Pagination<Category> findAll(CategorySearchQuery aQuery);

}

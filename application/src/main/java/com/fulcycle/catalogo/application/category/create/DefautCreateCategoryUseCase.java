package com.fulcycle.catalogo.application.category.create;

import com.fulcycle.catalogo.domain.category.Category;
import com.fulcycle.catalogo.domain.category.CategoryGatway;
import com.fulcycle.catalogo.domain.validation.Handler.Notification;
import com.fulcycle.catalogo.domain.validation.Handler.ThrowsValidationHandler;
import io.vavr.API;
import io.vavr.control.Either;

import java.util.Objects;

import static io.vavr.API.Left;
import static io.vavr.API.Try;

public class DefautCreateCategoryUseCase extends CreateCategoryUseCase {

    private final CategoryGatway categoryGatway;

    public DefautCreateCategoryUseCase(final CategoryGatway categoryGatway) {
        this.categoryGatway = Objects.requireNonNull(categoryGatway);
    }

    @Override
    public Either<Notification, CreateCategoryOutput> execute(final CreateCategoryCommand aCommand) {
        final var aName = aCommand.name();
        final var aDescription = aCommand.description();
        final var isActive = aCommand.isActive();
        final var notification = Notification.create();
        final var aCategory = Category.newCategory(aName, aDescription, isActive);

        aCategory.validate(notification);
        return notification.hasErrors() ? Left(notification) : create(aCategory);
    }

    private Either<Notification, CreateCategoryOutput> create(Category aCategory) {
      return Try(()->this.categoryGatway.create(aCategory))
                .toEither()
                .bimap(Notification::create,CreateCategoryOutput::from);
    }
}

package com.fulcycle.catalogo.application.category.update;

import com.fulcycle.catalogo.domain.category.Category;
import com.fulcycle.catalogo.domain.category.CategoryGatway;
import com.fulcycle.catalogo.domain.category.CategoryID;
import com.fulcycle.catalogo.domain.exeptions.DomainExeption;
import com.fulcycle.catalogo.domain.validation.Error;
import com.fulcycle.catalogo.domain.validation.Handler.Notification;
import io.vavr.API;
import io.vavr.control.Either;

import java.util.function.Supplier;

import static io.vavr.API.Left;

public class DefaultUpdateCategoryUseCase extends UpdateCategoryUseCase {

    private final CategoryGatway categoryGatway;

    public DefaultUpdateCategoryUseCase(CategoryGatway categoryGatway) {
        this.categoryGatway = categoryGatway;
    }

    @Override
    public Either<Notification, UpdateCategoryOutPut> execute(UpdateCategoryCommand aCommand) {
        final var anId = CategoryID.from(aCommand.id());
        final var anName = aCommand.name();
        final var anDescription = aCommand.description();
        final var isActive = aCommand.isActive();

        final var notification = Notification.create();
        final var aCategory = this.categoryGatway.findById(anId).orElseThrow(notFound(anId));

        aCategory
                .update(anName, anDescription, isActive)
                .validate(notification);
        return notification.hasErrors() ? Left(notification) : update(aCategory);
    }

    private Either<Notification, UpdateCategoryOutPut> update(final Category aCategory) {
        return API.Try(() -> this.categoryGatway.update(aCategory))
                .toEither()
                .bimap(Notification::create, UpdateCategoryOutPut::from);
    }

    private static Supplier<DomainExeption> notFound(CategoryID anId) {
        return () -> DomainExeption.with(new Error("Category with ID %s was not found".formatted(anId.getValue())));
    }
}

package com.fulcycle.catalogo.application.category.retrieve.get;

import com.fulcycle.catalogo.application.UseCase;

public abstract class GetCategoryByIdUseCase extends UseCase<String,CategoryOutput>{
}

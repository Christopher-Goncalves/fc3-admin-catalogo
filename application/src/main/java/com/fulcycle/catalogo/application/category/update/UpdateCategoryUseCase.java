package com.fulcycle.catalogo.application.category.update;

import com.fulcycle.catalogo.application.UseCase;
import com.fulcycle.catalogo.domain.validation.Handler.Notification;
import io.vavr.control.Either;

public abstract class UpdateCategoryUseCase extends UseCase<UpdateCategoryCommand, Either<Notification, UpdateCategoryOutPut>> {
}

package com.fulcycle.catalogo.application.category.create;

import com.fulcycle.catalogo.domain.category.Category;
import com.fulcycle.catalogo.domain.category.CategoryID;

public record CreateCategoryOutput(
        CategoryID id
) {

    public static CreateCategoryOutput from(final Category aCategory) {
        return new CreateCategoryOutput(aCategory.getId());
    }
}

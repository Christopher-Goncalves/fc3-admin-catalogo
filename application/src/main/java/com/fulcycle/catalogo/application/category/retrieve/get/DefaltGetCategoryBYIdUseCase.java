package com.fulcycle.catalogo.application.category.retrieve.get;

import com.fulcycle.catalogo.domain.category.CategoryGatway;
import com.fulcycle.catalogo.domain.category.CategoryID;
import com.fulcycle.catalogo.domain.exeptions.DomainExeption;
import com.fulcycle.catalogo.domain.validation.Error;

import java.util.Objects;
import java.util.function.Supplier;

public class DefaltGetCategoryBYIdUseCase extends GetCategoryByIdUseCase {

    private final CategoryGatway categoryGatway;

    public DefaltGetCategoryBYIdUseCase(CategoryGatway categoryGatway) {
        this.categoryGatway = Objects.requireNonNull(categoryGatway);
    }

    @Override
    public CategoryOutput execute(String anIn) {

        final var anCategoryId = CategoryID.from(anIn);
        return this.categoryGatway.findById(anCategoryId)
                .map(CategoryOutput::from)
                .orElseThrow(notFound(anCategoryId));
    }

    private static Supplier<DomainExeption> notFound(CategoryID anId) {
        return () -> DomainExeption.with(new Error("Category with ID %s was not found".formatted(anId.getValue())));
    }
}

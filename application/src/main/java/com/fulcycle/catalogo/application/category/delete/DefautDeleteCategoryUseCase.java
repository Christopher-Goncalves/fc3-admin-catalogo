package com.fulcycle.catalogo.application.category.delete;

import com.fulcycle.catalogo.domain.category.CategoryGatway;
import com.fulcycle.catalogo.domain.category.CategoryID;

import java.util.Objects;

public class DefautDeleteCategoryUseCase extends DeleteCategoryUseCase {

    private final CategoryGatway categoryGatway;

    public DefautDeleteCategoryUseCase(CategoryGatway categoryGatway) {
        this.categoryGatway = Objects.requireNonNull(categoryGatway);
    }

    @Override
    public void execute(final String anIn) {
        this.categoryGatway.deleteById(CategoryID.from(anIn));
    }
}

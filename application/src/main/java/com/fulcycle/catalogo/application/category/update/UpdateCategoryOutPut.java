package com.fulcycle.catalogo.application.category.update;

import com.fulcycle.catalogo.domain.category.Category;
import com.fulcycle.catalogo.domain.category.CategoryID;

public record UpdateCategoryOutPut(
        CategoryID id
) {
    public static UpdateCategoryOutPut from(final Category aCategory) {
        return new UpdateCategoryOutPut(aCategory.getId());
    }
}

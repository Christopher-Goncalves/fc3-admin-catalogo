package com.fulcycle.catalogo.application.category.retrieve.list;

import com.fulcycle.catalogo.application.UseCase;
import com.fulcycle.catalogo.domain.category.CategorySearchQuery;
import com.fulcycle.catalogo.domain.pagination.Pagination;

public abstract class ListCategoriesUseCase extends UseCase<CategorySearchQuery, Pagination<CategoryListOutPut>> {
}

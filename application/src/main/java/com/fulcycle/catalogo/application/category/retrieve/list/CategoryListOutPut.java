package com.fulcycle.catalogo.application.category.retrieve.list;

import com.fulcycle.catalogo.domain.category.Category;
import com.fulcycle.catalogo.domain.category.CategoryID;

import java.time.Instant;

public record CategoryListOutPut(
        CategoryID id,
        String name,
        String description,
        boolean isActive,
        Instant createdAt,
        Instant DeletedAt
) {

    public static CategoryListOutPut from(final Category aCategory) {
        return new CategoryListOutPut(
                aCategory.getId(),
                aCategory.getName(),
                aCategory.getDescription(),
                aCategory.getActive(),
                aCategory.getCreatedAt(),
                aCategory.getDeletedAt()
        );
    }
}

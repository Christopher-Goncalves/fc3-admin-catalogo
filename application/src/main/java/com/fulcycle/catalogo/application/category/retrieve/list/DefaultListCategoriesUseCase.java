package com.fulcycle.catalogo.application.category.retrieve.list;

import com.fulcycle.catalogo.domain.category.CategoryGatway;
import com.fulcycle.catalogo.domain.category.CategorySearchQuery;
import com.fulcycle.catalogo.domain.pagination.Pagination;

import java.util.Objects;

public class DefaultListCategoriesUseCase extends ListCategoriesUseCase {
    private final CategoryGatway categoryGatway;

    public DefaultListCategoriesUseCase(CategoryGatway categoryGatway) {
        this.categoryGatway = Objects.requireNonNull(categoryGatway);
    }

    @Override
    public Pagination<CategoryListOutPut> execute(CategorySearchQuery aQuery) {

        return this.categoryGatway.findAll(aQuery)
                .map(CategoryListOutPut::from);
    }
}

package com.fulcycle.catalogo.application.category.create;

import com.fulcycle.catalogo.application.UseCase;
import com.fulcycle.catalogo.domain.validation.Handler.Notification;
import io.vavr.control.Either;

public abstract class CreateCategoryUseCase extends UseCase<CreateCategoryCommand, Either<Notification, CreateCategoryOutput>> {

}

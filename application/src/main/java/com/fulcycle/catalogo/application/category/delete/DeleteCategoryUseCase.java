package com.fulcycle.catalogo.application.category.delete;

import com.fulcycle.catalogo.application.UnitUseCase;

public abstract class DeleteCategoryUseCase extends UnitUseCase<String> {
}

package com.fulcycle.catalogo.application.category.delete;


import com.fulcycle.catalogo.application.category.delete.DefautDeleteCategoryUseCase;
import com.fulcycle.catalogo.domain.category.Category;
import com.fulcycle.catalogo.domain.category.CategoryGatway;
import com.fulcycle.catalogo.domain.category.CategoryID;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class DeleteCategoryUseCaseTest {

    @InjectMocks
    private DefautDeleteCategoryUseCase useCase;
    @Mock
    private CategoryGatway categoryGatway;

    @BeforeEach
    void cleanUp() {
        Mockito.reset(categoryGatway);
    }

    // 1 caminho feliz
    // 2 id invalido
    // 3 erro de gatway

    @Test
    public void givenAValidCommand_whenCallsDeleteCategory_shouldOk() {
        final var aCategory = Category.newCategory("Film", null, true);
        final var expectedId = aCategory.getId();

        doNothing().when(categoryGatway).deleteById(expectedId);

        Assertions.assertDoesNotThrow(() -> useCase.execute(expectedId.getValue()));

        Mockito.verify(categoryGatway, times(1)).deleteById(eq(expectedId));
    }

    @Test
    public void givenInvalidId_whenCallsDeleteCategory_shouldOK() {
//        final var aCategory = Category.newCategory("Film", null, true);
        final var expectedId = CategoryID.from("123");

        doNothing().when(categoryGatway).deleteById(expectedId);

        Assertions.assertDoesNotThrow(() -> useCase.execute(expectedId.getValue()));

        Mockito.verify(categoryGatway, times(1)).deleteById(eq(expectedId));

    }

    @Test
    public void givenValidId_whenGatewayThorwsException_shouldReturnException() {
        final var aCategory = Category.newCategory("Film", null, true);
        final var expectedId = aCategory.getId();

        doThrow(new IllegalStateException("Gatway error")).when(categoryGatway).deleteById(expectedId);

        Assertions.assertThrows(IllegalStateException.class, () -> useCase.execute(expectedId.getValue()));

        Mockito.verify(categoryGatway, times(1)).deleteById(eq(expectedId));
    }
}

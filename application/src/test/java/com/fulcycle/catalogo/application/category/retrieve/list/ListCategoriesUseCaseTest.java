package com.fulcycle.catalogo.application.category.retrieve.list;

import com.fulcycle.catalogo.domain.category.Category;
import com.fulcycle.catalogo.domain.category.CategoryGatway;
import com.fulcycle.catalogo.domain.category.CategorySearchQuery;
import com.fulcycle.catalogo.domain.pagination.Pagination;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ListCategoriesUseCaseTest {
    @InjectMocks
    private DefaultListCategoriesUseCase useCase;
    @Mock
    private CategoryGatway categoryGatway;

    @BeforeEach
    void cleanUp() {
        Mockito.reset(categoryGatway);
    }

    @Test
    public void givenAValidQuery_whenCallsListCategories_thenShouldReturnCategories() {
        final var categories = List.of(Category.newCategory("Filmes", null, true),
                Category.newCategory("Series", null, true)
        );

        final var expectedPage = 0;
        final var expectedPerPage = 10;
        final var expectedTerms = "";
        final var expectedSort = "createdAt";
        final var ecpectedDirection = "asc";


        final var aQuery = new CategorySearchQuery(expectedPage, expectedPerPage, expectedTerms, expectedSort, ecpectedDirection);


        final var expectedPagination = new Pagination<>(expectedPage, expectedPerPage, categories.size(), categories);

        final var expectedItemsCount = 2;
        final var expectedResult = expectedPagination.map(CategoryListOutPut::from);

        when(categoryGatway.findAll(eq(aQuery))).thenReturn(expectedPagination);

        final var actialResult = useCase.execute(aQuery);
        Assertions.assertEquals(expectedItemsCount, actialResult.items().size());
        Assertions.assertEquals(expectedResult, actialResult);
        Assertions.assertEquals(expectedPage, actialResult.currentPage());
        Assertions.assertEquals(expectedPerPage, actialResult.perPage());
        Assertions.assertEquals(categories.size(), actialResult.total());
    }

    @Test
    public void givenAValidQuery_whenHasNoResults_thenShouldEmptyCategories() {
        final var categories = List.<Category>of();

        final var expectedPage = 0;
        final var expectedPerPage = 10;
        final var expectedTerms = "";
        final var expectedSort = "createdAt";
        final var ecpectedDirection = "asc";


        final var aQuery = new CategorySearchQuery(expectedPage, expectedPerPage, expectedTerms, expectedSort, ecpectedDirection);


        final var expectedPagination = new Pagination<>(expectedPage, expectedPerPage, categories.size(), categories);

        final var expectedItemsCount = 0;
        final var expectedResult = expectedPagination.map(CategoryListOutPut::from);

        when(categoryGatway.findAll(eq(aQuery))).thenReturn(expectedPagination);

        final var actialResult = useCase.execute(aQuery);
        Assertions.assertEquals(expectedItemsCount, actialResult.items().size());
        Assertions.assertEquals(expectedResult, actialResult);
        Assertions.assertEquals(expectedPage, actialResult.currentPage());
        Assertions.assertEquals(expectedPerPage, actialResult.perPage());
        Assertions.assertEquals(categories.size(), actialResult.total());
    }

    @Test
    public void givenAValidQuery_whenGatewayThorwsException_ShouldReturnException() {
        final var expectedPage = 0;
        final var expectedPerPage = 10;
        final var expectedTerms = "";
        final var expectedSort = "createdAt";
        final var ecpectedDirection = "asc";
        final var expectedMessageError = "Gatway Error";

        final var aQuery = new CategorySearchQuery(expectedPage, expectedPerPage, expectedTerms, expectedSort, ecpectedDirection);

        when(categoryGatway.findAll(eq(aQuery))).thenThrow(new IllegalStateException(expectedMessageError));

        final var actialException = Assertions.assertThrows(IllegalStateException.class, () -> useCase.execute(aQuery));

        Assertions.assertEquals(expectedMessageError, actialException.getMessage());

    }
}

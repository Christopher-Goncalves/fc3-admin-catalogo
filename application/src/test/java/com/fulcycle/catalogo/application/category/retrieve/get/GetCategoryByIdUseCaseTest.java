package com.fulcycle.catalogo.application.category.retrieve.get;

import com.fulcycle.catalogo.domain.category.Category;
import com.fulcycle.catalogo.domain.category.CategoryGatway;
import com.fulcycle.catalogo.domain.category.CategoryID;
import com.fulcycle.catalogo.domain.exeptions.DomainExeption;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class GetCategoryByIdUseCaseTest {
    @InjectMocks
    private DefaltGetCategoryBYIdUseCase useCase;
    @Mock
    private CategoryGatway categoryGatway;

    @BeforeEach
    void cleanUp() {
        Mockito.reset(categoryGatway);
    }

    @Test
    public void givenAValidCommand_whenCallsGetCategory_shoulReturnCategory() {
        final var expectedName = "Filmes";
        final var expectedDescription = "A categoria mais assistida";
        final var expectedIsActive = true;
        final var acategory = Category.newCategory(expectedName, expectedDescription, expectedIsActive);
        final var expectedId = acategory.getId();

        when(categoryGatway.findById(eq(expectedId))).thenReturn(Optional.of(acategory.clone()));

        final var actualCategory = useCase.execute(expectedId.getValue());

        Assertions.assertEquals(expectedId, actualCategory.id());
        Assertions.assertEquals(expectedName, actualCategory.name());
        Assertions.assertEquals(expectedDescription, actualCategory.description());
        Assertions.assertEquals(acategory.getCreatedAt(), actualCategory.createdAt());
        Assertions.assertEquals(acategory.getUpdatedAt(), actualCategory.updatedAt());
        Assertions.assertEquals(acategory.getDeletedAt(), actualCategory.deletedAt());

    }

    @Test
    public void givenInvalidId_whenCallsGetCategory_shouldReturnNotFound() {
        final var expectedErrorMessage = "Category with ID 123 was not found";

        final var expectedId = CategoryID.from("123");

        when(categoryGatway.findById(eq(expectedId))).thenReturn(Optional.empty());
        final var actualExeption = Assertions.assertThrows(
                DomainExeption.class,
                () -> useCase.execute(expectedId.getValue())
        );

        Assertions.assertEquals(expectedErrorMessage, actualExeption.getMessage());
    }


    @Test
    public void givenValidId_whenGatewayThorwsException_shouldReturnException() {
        final var expectedErrorMessage = "Gateway error";
        final var expectedId = CategoryID.from("123");

        when(categoryGatway.findById(eq(expectedId))).thenThrow(new IllegalStateException(expectedErrorMessage));
       final var actualExeption = Assertions.assertThrows(
                IllegalStateException.class,
                () -> useCase.execute(expectedId.getValue())
        );
        Assertions.assertEquals(expectedErrorMessage, actualExeption.getMessage());
    }

}

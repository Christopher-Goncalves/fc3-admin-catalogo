package com.fulcycle.catalogo.application.category.update;

import com.fulcycle.catalogo.application.category.create.CreateCategoryCommand;
import com.fulcycle.catalogo.application.category.create.DefautCreateCategoryUseCase;
import com.fulcycle.catalogo.domain.category.Category;
import com.fulcycle.catalogo.domain.category.CategoryGatway;
import com.fulcycle.catalogo.domain.category.CategoryID;
import com.fulcycle.catalogo.domain.exeptions.DomainExeption;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Objects;
import java.util.Optional;

import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class UpdateCategoryUseCaseTeste {
    // 1 teste do caminho felix
    //2 teste passando propiedade invalida(name)
    // 3 teste atualizando uma categocia invalida
    //4 teste simulando um erro generico vindo do gateway
    // 5 atualizando categoria passando id invalido
    @BeforeEach
    void cleanUp() {
        Mockito.reset(categoryGatway);
    }

    @InjectMocks
    private DefaultUpdateCategoryUseCase useCase;
    @Mock
    private CategoryGatway categoryGatway;

    @Test
    public void givenAValidCommand_whenCallsUpdateCategory_shoulReturnCategoryId() {
        final var expectedName = "Filmes";
        final var expectedDescription = "A categoria mais assistida";
        final var expectedIsActive = true;

        final var aCategory = Category.newCategory("Film", null, true);
        final var expectedId = aCategory.getId();

        final var aCommand = UpdateCategoryCommand.with(
                aCategory.getId().getValue(),
                expectedName,
                expectedDescription,
                expectedIsActive
        );
        when(categoryGatway.findById(eq(expectedId)))
                .thenReturn(Optional.of(aCategory.clone()));

        when(categoryGatway.update(any())).thenAnswer(returnsFirstArg());

        final var actualOutput = useCase.execute(aCommand).get();
        Assertions.assertNotNull(actualOutput);
        Assertions.assertNotNull(actualOutput.id());

        Mockito.verify(categoryGatway, times(1)).findById(expectedId);
        Mockito.verify(categoryGatway, times(1)).update(
                argThat(aUpdatedCategory ->
                        Objects.equals(expectedName, aUpdatedCategory.getName())
                                && Objects.equals(expectedDescription, aUpdatedCategory.getDescription())
                                && Objects.equals(expectedIsActive, aUpdatedCategory.getActive())
                                && Objects.equals(expectedId, aUpdatedCategory.getId())
                                && Objects.equals(aCategory.getCreatedAt(), aUpdatedCategory.getCreatedAt())
                                && aCategory.getUpdatedAt().isBefore(aUpdatedCategory.getUpdatedAt())
                                && Objects.isNull(aUpdatedCategory.getDeletedAt())
                ));

    }

    @Test
    public void givenAInvalidName_whenCallsUpdateCategory_shoulReturnDomainExeption() {
        final var aCategory = Category.newCategory("Film", null, true);

        final var expectedId = aCategory.getId();
        final String expectedName = null;
        final var expectedDescription = "A categoria mais assistida";
        final var expectedIsActive = true;

        final var expectedErrorMessage = "'name' should not be null";
        final var expectedErrorCount = 1;


        final var aCommand = UpdateCategoryCommand.with(
                aCategory.getId().getValue(),
                expectedName,
                expectedDescription,
                expectedIsActive
        );

        when(categoryGatway.findById(eq(expectedId)))
                .thenReturn(Optional.of(aCategory.clone()));

        final var notification = useCase.execute(aCommand).getLeft();

        Assertions.assertEquals(expectedErrorCount, notification.getErrors().size());
        Assertions.assertEquals(expectedErrorMessage, notification.firstErro().message());

        Mockito.verify(categoryGatway, times(0)).create(any());

    }

    @Test
    public void givenAValidCommandWithInactiveCategory_whenCallsCreateCategory_shoulReturnInactiveCategoryId() {
        final var aCategory = Category.newCategory("Film", null, true);


        final var expectedId = aCategory.getId();
        final var expectedName = "Filmes";
        final var expectedDescription = "A categoria mais assistida";
        final var expectedIsActive = false;


        final var aCommand = UpdateCategoryCommand.with(
                aCategory.getId().getValue(),
                expectedName,
                expectedDescription,
                expectedIsActive
        );
        when(categoryGatway.findById(eq(expectedId)))
                .thenReturn(Optional.of(aCategory.clone()));

        when(categoryGatway.update(any())).thenAnswer(returnsFirstArg());

        Assertions.assertTrue(aCategory.getActive());
        Assertions.assertNull(aCategory.getDeletedAt());

        final var actualOutput = useCase.execute(aCommand).get();
        Assertions.assertNotNull(actualOutput);
        Assertions.assertNotNull(actualOutput.id());

        Mockito.verify(categoryGatway, times(1)).findById(expectedId);
        Mockito.verify(categoryGatway, times(1)).update(
                argThat(aUpdatedCategory ->
                        Objects.equals(expectedName, aUpdatedCategory.getName())
                                && Objects.equals(expectedDescription, aUpdatedCategory.getDescription())
                                && Objects.equals(expectedIsActive, aUpdatedCategory.getActive())
                                && Objects.equals(expectedId, aUpdatedCategory.getId())
                                && Objects.equals(aCategory.getCreatedAt(), aUpdatedCategory.getCreatedAt())
                                && aCategory.getUpdatedAt().isBefore(aUpdatedCategory.getUpdatedAt())
                                && Objects.nonNull(aUpdatedCategory.getDeletedAt())
                ));

    }

    @Test
    public void givenAValidCommand_whenGatewayThrowsRandomExeption_shoulReturnAExeptin() {
        final var aCategory = Category.newCategory("Film", null, true);

        final var expectedId = aCategory.getId();
        final var expectedName = "Filmes";
        final var expectedDescription = "A categoria mais assistida";
        final var expectedIsActive = true;

        final var expectErrorMessage = "Gatway Error";
        final var expectedErrorCount = 1;

        final var aCommand = UpdateCategoryCommand.with(
                aCategory.getId().getValue(),
                expectedName,
                expectedDescription,
                expectedIsActive
        );

        when(categoryGatway.findById(eq(expectedId)))
                .thenReturn(Optional.of(aCategory.clone()));

        when(categoryGatway.update(Mockito.any()))
                .thenThrow(new IllegalStateException("Gatway Error"));

        final var notification = useCase.execute(aCommand).getLeft();

        Assertions.assertEquals(expectedErrorCount, notification.getErrors().size());
        Assertions.assertEquals(expectErrorMessage, notification.firstErro().message());

        Mockito.verify(categoryGatway, times(1)).update(
                argThat(aUpdatedCategory ->
                        Objects.equals(expectedName, aUpdatedCategory.getName())
                                && Objects.equals(expectedDescription, aUpdatedCategory.getDescription())
                                && Objects.equals(expectedIsActive, aUpdatedCategory.getActive())
                                && Objects.equals(expectedId, aUpdatedCategory.getId())
                                && Objects.equals(aCategory.getCreatedAt(), aUpdatedCategory.getCreatedAt())
                                && aCategory.getUpdatedAt().isBefore(aUpdatedCategory.getUpdatedAt())
                                && Objects.isNull(aUpdatedCategory.getDeletedAt())
                ));
    }

    @Test
    public void givenACommandWithInvalidID_whenCallsUpdateCategory_shoulReturnNotFoundExeption() {
        final var expectedName = "Filmes";
        final var expectedDescription = "A categoria mais assistida";
        final var expectedIsActive = true;
        final var expectedId = "123";

        final var expectErrorMessage = "Category with ID 123 was not found";
        final var expectedErrorCount = 1;

        final var aCommand = UpdateCategoryCommand.with(
                expectedId,
                expectedName,
                expectedDescription,
                expectedIsActive
        );
        when(categoryGatway.findById(eq(CategoryID.from(expectedId))))
                .thenReturn(Optional.empty());

        final var actualExeption = Assertions.assertThrows(DomainExeption.class, () -> useCase.execute(aCommand).get());
        Assertions.assertEquals(expectErrorMessage, actualExeption.getErros().get(0).message());
        Assertions.assertEquals(expectedErrorCount, actualExeption.getErros().size());

        Mockito.verify(categoryGatway, times(1)).findById(CategoryID.from(expectedId));
        Mockito.verify(categoryGatway, times(0)).update(any());

    }

}

package com.fulcycle.catalogo.infrastructure.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.fulcycle.catalogo")
public class WebServerConfig {
}

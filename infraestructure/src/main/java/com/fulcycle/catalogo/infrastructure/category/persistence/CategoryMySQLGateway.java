package com.fulcycle.catalogo.infrastructure.category.persistence;

import com.fulcycle.catalogo.domain.category.Category;
import com.fulcycle.catalogo.domain.category.CategoryGatway;
import com.fulcycle.catalogo.domain.category.CategoryID;
import com.fulcycle.catalogo.domain.category.CategorySearchQuery;
import com.fulcycle.catalogo.domain.pagination.Pagination;
import com.fulcycle.catalogo.infrastructure.utils.SpecificationUtils;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.Optional;

import static com.fulcycle.catalogo.infrastructure.utils.SpecificationUtils.like;
import static org.springframework.data.domain.Sort.*;

@Service
public class CategoryMySQLGateway implements CategoryGatway {
    private CategoryRepository repository;

    public CategoryMySQLGateway(final CategoryRepository repository) {
        this.repository = repository;
    }

    @Override
    public Category create(final Category aCategory) {

        return save(aCategory);
    }

    @Override
    public void deleteById(final CategoryID anId) {
        final var anIdValue = anId.getValue();
        if (this.repository.existsById(anIdValue)) {
            this.repository.deleteById(anIdValue);
        }
    }

    @Override
    public Optional<Category> findById(final CategoryID anId) {
        return this.repository.findById(anId.getValue()).map(CategoryJpaEntity::toAgragate);
    }

    @Override
    public Category update(final Category aCategory) {

        return save(aCategory);
    }

    private Category save(final Category aCategory) {
        return this.repository.save(CategoryJpaEntity.from(aCategory)).toAgragate();
    }

    @Override
    public Pagination<Category> findAll(final CategorySearchQuery aQuery) {
        //paginação
        final var page = PageRequest.of(
                aQuery.page(),
                aQuery.perPage(),
                Sort.by(Direction.fromString(aQuery.direction()), aQuery.sort())
        );

        //busca dimanima pelo criterio terms(nome ou description)
        final var specifications = Optional.ofNullable(aQuery.terms())
                .filter(str -> !str.isBlank())
                .map(str ->
                        SpecificationUtils
                                .<CategoryJpaEntity>like("name", str)
                                .or(like("description", str))
                )
                .orElse(null);

        final var pageResult = this.repository.findAll(Specification.where(specifications), page);


        return new Pagination<>(
                pageResult.getNumber(),
                pageResult.getSize(),
                pageResult.getTotalElements(),
                pageResult.map(CategoryJpaEntity::toAgragate).toList()
        );
    }
}

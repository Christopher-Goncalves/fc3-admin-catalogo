package com.fulcycle.catalogo.infrastructure.configuration.usecases;

import com.fulcycle.catalogo.application.category.create.CreateCategoryUseCase;
import com.fulcycle.catalogo.application.category.create.DefautCreateCategoryUseCase;
import com.fulcycle.catalogo.application.category.delete.DefautDeleteCategoryUseCase;
import com.fulcycle.catalogo.application.category.delete.DeleteCategoryUseCase;
import com.fulcycle.catalogo.application.category.retrieve.get.DefaltGetCategoryBYIdUseCase;
import com.fulcycle.catalogo.application.category.retrieve.get.GetCategoryByIdUseCase;
import com.fulcycle.catalogo.application.category.retrieve.list.DefaultListCategoriesUseCase;
import com.fulcycle.catalogo.application.category.retrieve.list.ListCategoriesUseCase;
import com.fulcycle.catalogo.application.category.update.DefaultUpdateCategoryUseCase;
import com.fulcycle.catalogo.application.category.update.UpdateCategoryUseCase;
import com.fulcycle.catalogo.domain.category.CategoryGatway;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CategoryUsecaseConfig {

    private final CategoryGatway categoryGatway;

    public CategoryUsecaseConfig(final CategoryGatway categoryGatway) {
        this.categoryGatway = categoryGatway;
    }

    @Bean
    public CreateCategoryUseCase createCategoryUseCase() {
        return new DefautCreateCategoryUseCase(categoryGatway);
    }

    @Bean
    public UpdateCategoryUseCase updateCategoryUseCase() {
        return new DefaultUpdateCategoryUseCase(categoryGatway);
    }

    @Bean
    public GetCategoryByIdUseCase getCategoryByIdUseCase() {
        return new DefaltGetCategoryBYIdUseCase(categoryGatway);
    }

    @Bean
    public ListCategoriesUseCase listCategoriesUseCase() {
        return new DefaultListCategoriesUseCase(categoryGatway);
    }

    @Bean
    public DeleteCategoryUseCase deleteCategoryUseCase() {
        return new DefautDeleteCategoryUseCase(categoryGatway);
    }


}

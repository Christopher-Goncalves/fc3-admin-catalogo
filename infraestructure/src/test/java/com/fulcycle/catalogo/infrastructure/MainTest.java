package com.fulcycle.catalogo.infrastructure;

import com.fulcycle.catalogo.application.UseCase;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.core.env.AbstractEnvironment;

public class MainTest {

    @Test
    public void TestMain() {
        Assertions.assertNotNull(new Main());
        System.setProperty(AbstractEnvironment.DEFAULT_PROFILES_PROPERTY_NAME , "test");
        Main.main(new String[]{});
    }
}

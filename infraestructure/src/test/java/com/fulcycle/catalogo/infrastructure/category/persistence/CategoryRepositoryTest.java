package com.fulcycle.catalogo.infrastructure.category.persistence;

import com.fulcycle.catalogo.domain.category.Category;
import com.fulcycle.catalogo.MySQLGatewayTest;
import org.hibernate.PropertyValueException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;

@MySQLGatewayTest
public class CategoryRepositoryTest {
    @Autowired
    private CategoryRepository categoryRepository;

    @Test
    public void givenAnInvalidNUllName_whenCallsSave_shouldReturnError() {
        final var expectedPropetyName = "name";
        final var expectedErrorMessage = "not-null property references a null or transient value : com.fulcycle.catalogo.infrastructure.category.persistence.CategoryJpaEntity.name";

        final var aCategory = Category.newCategory("Filmes", "A Categoria mais assistinda", true);
        final var anEntity = CategoryJpaEntity.from(aCategory);

        anEntity.setName(null);

        final var actualException =
                Assertions.assertThrows(DataIntegrityViolationException.class, () -> categoryRepository.save(anEntity));

        final var actualCause = Assertions.assertInstanceOf(PropertyValueException.class, actualException.getCause());

        Assertions.assertEquals(expectedPropetyName, actualCause.getPropertyName());
        Assertions.assertEquals(expectedErrorMessage, actualCause.getMessage());

    }

    @Test
    public void givenAnInvalidNUllCreatedAt_whenCallsSave_shouldReturnError() {
        final var expectedPropetyName = "createdAt";
        final var expectedErrorMessage = "not-null property references a null or transient value : com.fulcycle.catalogo.infrastructure.category.persistence.CategoryJpaEntity.createdAt";

        final var aCategory = Category.newCategory("Filmes", "A Categoria mais assistinda", true);
        final var anEntity = CategoryJpaEntity.from(aCategory);

        anEntity.setCreatedAt(null);

        final var actualException =
                Assertions.assertThrows(DataIntegrityViolationException.class, () -> categoryRepository.save(anEntity));

        final var actualCause = Assertions.assertInstanceOf(PropertyValueException.class, actualException.getCause());

        Assertions.assertEquals(expectedPropetyName, actualCause.getPropertyName());
        Assertions.assertEquals(expectedErrorMessage, actualCause.getMessage());

    }
    @Test
    public void givenAnInvalidNUllUpdatedAt_whenCallsSave_shouldReturnError() {
        final var expectedPropetyName = "updatedAt";
        final var expectedErrorMessage = "not-null property references a null or transient value : com.fulcycle.catalogo.infrastructure.category.persistence.CategoryJpaEntity.updatedAt";

        final var aCategory = Category.newCategory("Filmes", "A Categoria mais assistinda", true);
        final var anEntity = CategoryJpaEntity.from(aCategory);

        anEntity.setUpdatedAt(null);

        final var actualException =
                Assertions.assertThrows(DataIntegrityViolationException.class, () -> categoryRepository.save(anEntity));

        final var actualCause = Assertions.assertInstanceOf(PropertyValueException.class, actualException.getCause());

        Assertions.assertEquals(expectedPropetyName, actualCause.getPropertyName());
        Assertions.assertEquals(expectedErrorMessage, actualCause.getMessage());

    }
}

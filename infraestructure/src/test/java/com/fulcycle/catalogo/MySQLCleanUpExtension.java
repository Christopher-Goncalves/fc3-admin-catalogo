package com.fulcycle.catalogo;


import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.springframework.data.repository.CrudRepository;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Collection;

public class MySQLCleanUpExtension implements BeforeEachCallback {

    @Override
    public void beforeEach(final ExtensionContext context) {

        cleanUp( SpringExtension.getApplicationContext(context)
                .getBeansOfType(CrudRepository.class)
                .values());

//        final var appContext = SpringExtension.getApplicationContext(context);
//        cleanUp(List.of(
//                appContext.getBean(VideoRepository.class),
//                appContext.getBean(CastMemberRepository.class),
//                appContext.getBean(GenreRepository.class),
//                appContext.getBean(CategoryRepository.class)
//        ));
    }

    private void cleanUp(final Collection<CrudRepository> repositories) {
        repositories.forEach(CrudRepository::deleteAll);
    }
}